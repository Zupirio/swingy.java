package com.arangari.swingy.view;

public interface ViewMode {
    public void run();
}