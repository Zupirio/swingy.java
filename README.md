# swingy
 This is the second project from the Java world at 42. You will learn to develop GUI applications with the SWING framework, in order to create an RPG game.
# Swingy.java
This project uses maven and can be executed as 
 ### java -jar target/swingy-1.0-SNAPSHOT.jar (console / gui)
 ---
* console - to run the program on the console
* gui - to run the program in the gui